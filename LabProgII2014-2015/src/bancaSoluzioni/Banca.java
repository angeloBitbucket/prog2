package bancaSoluzioni;

import java.util.Vector;

public class Banca {

	private final String nomeBanca;
	private Vector< ContoCorrente > elencoConti;

	// //////////////////////////////////
	public Banca(String nomeBanca) {
		this.nomeBanca = nomeBanca;
		elencoConti = new Vector<ContoCorrente>();
	}

	public int apriContoCorrente(String nomeCorrentista, double versamento) {
		ContoCorrente conto = new ContoCorrente(nomeCorrentista, versamento);

		elencoConti.add(conto);
		return conto.getNumeroConto();
	}

	public int apriContoCorrente(String nomeCorrentista) {
		return apriContoCorrente(nomeCorrentista, 0);
	}

	private int indiceConto(int numeroConto) {

		for (int i = 0; i < elencoConti.size(); i++) {
			if (elencoConti.get(i).getNumeroConto() == numeroConto) {
				return i;
			}
		}
		return -1;
	}

	public boolean deposita(int numeroConto, double valore) {

		int numInterno = indiceConto(numeroConto);
		if (numInterno == -1 || valore <= 0) {
			return false;
		}
		elencoConti.get(numInterno).deposita(valore);
		return true;
	}

	public boolean preleva(int numeroConto, double valore) {
		int numInterno = indiceConto(numeroConto);
		if (numInterno == -1 || valore <= 0) {
			return false;
		}

		if (elencoConti.get(numInterno).preleva(valore)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean trasferisci(int daNumeroConto, int aNumeroConto,
			double valore) {
		return preleva(daNumeroConto, valore) && deposita(aNumeroConto, valore);
	}

	public void selectionSort(boolean ordinaPerSaldo) {
		for (int i = 0; i < elencoConti.size() - 1; i++) {
			int posMin = posizioneMinimo(i, ordinaPerSaldo);
			scambia(posMin, i);
		}
	}

	private int posizioneMinimo(int i, boolean ordinaPerSaldo) {
		int posMin = i;
		for (int j = i; j < elencoConti.size(); j++) {
			if (elencoConti.get(j).minore(elencoConti.get(posMin),
					ordinaPerSaldo)) {
				posMin = j;
			}
		}
		return posMin;
	}

	private void scambia(int i, int j) {
		ContoCorrente temp = elencoConti.get(i);
		elencoConti.set(i, elencoConti.get(j));
		elencoConti.set(j, temp);
	}

	public String toString() {

		String temp = "";

		temp += "Nome Banca: " + nomeBanca + "\n";
		temp += "Conti Aperti: " + elencoConti.size() + "\n";

		temp += "Elenco Numeri di Conto -----------------------\n";
		for (int i = 0; i < elencoConti.size(); i++) {
			temp += elencoConti.get(i) + "";
		}
		temp += "Fine elenco -----------------------\n";

		return temp;
	}
}

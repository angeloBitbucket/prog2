package bancaSoluzioni;

public class ContoCorrente {

	private final int numeroConto;
	private String nomeCorrentista;
	private double saldo;
	private static int numerazioneConti = 0;

	public ContoCorrente(String nomeCorrentista, double versamento) {
		this.nomeCorrentista = nomeCorrentista;
		saldo = versamento;
		numeroConto = ++numerazioneConti;
	}

	public double getSaldo() {
		return saldo;
	}

	public int getNumeroConto() {
		return numeroConto;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public boolean deposita(double valore) {
		if (valore < 0) {
			return false;
		}
		saldo += valore;
		return true;
	}

	public boolean preleva(double valore) {
		if (valore < 0 || (saldo - valore) < 0) {
			return false;
		}
		saldo -= valore;
		return true;
	}

	public boolean minore(ContoCorrente cont, boolean usaSaldo) {
		if (usaSaldo)
			return getSaldo() < cont.getSaldo();
		else
			return getNomeCorrentista().compareTo(cont.getNomeCorrentista()) < 0;
	}

	public String toString() {

		String temp = "";

		temp += "Conto numero: " + numeroConto + ", ";
		temp += "Nome Correntista: " + nomeCorrentista + ", ";
		temp += "Saldo: " + saldo + "\n";

		return temp;
	}
}

package bancaSoluzioni;

public class TestBanca {

    public static void main(String[] args) {

        System.out.println("CHK1");
        ContoCorrente cc = new ContoCorrente("Luca", 121.30);

        System.out.println(cc);//System.out.println(cc.toString());
        System.out.println("CHK2");

        Banca bm = new Banca("BancoMio");
        Banca bp = new Banca("BancoPosta");

        System.out.println(bm);
        System.out.println(bp);

        int numContoMatteo = bm.apriContoCorrente("Matteo", 100.00);
        int numContoMario = bm.apriContoCorrente("Mario", 20.00);
        int numContoAliceBM = bm.apriContoCorrente("Alice", 1000.00);
        int numContoAliceBP = bp.apriContoCorrente("Alice", 2000.00);
        int numContoViola = bm.apriContoCorrente("Viola Bianchi", 120.30);

        System.out.println("CHK1:========= ");
        System.out.println(bm);
        System.out.println("cHk2:========= ");

        System.out.println(bm);
        System.out.println(bp);

        bm.deposita(numContoMatteo, 50.00);

        bm.deposita(numContoMatteo, 50.00);
        bm.deposita(numContoMario, 150.00);
        bm.deposita(numContoAliceBM, 150.00);
        bp.deposita(numContoAliceBP, 100.00);

        bm.preleva(numContoMatteo, 10);
        bm.trasferisci(numContoViola, numContoMatteo, 20);

        System.out.println(bm);
        System.out.println(bp);
        
        bm.selectionSort(false);
        System.out.println("\nCONTI ORDINATI PER CORRENTISTA:");
        System.out.println(bm);
        bm.selectionSort(true);
        System.out.println("\nCONTI ORDINATI PER SALDO:");
        System.out.println(bm);
    }
}
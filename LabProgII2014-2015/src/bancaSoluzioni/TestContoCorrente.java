package bancaSoluzioni;

public class TestContoCorrente {

    public static void main(String[] args) {
         ContoCorrente cc=new ContoCorrente("Lorenzo", 121.30);

         System.out.println(cc);//System.out.println(cc.toString());

         cc.preleva(10);

         System.out.println(cc);//System.out.println(cc.toString());

         cc.preleva(1000);

         System.out.println(cc);//System.out.println(cc.toString());

         cc.deposita(150.99);

         System.out.println(cc);//System.out.println(cc.toString());

         ContoCorrente cc2=new ContoCorrente("Matteo", 111.30);

         System.out.println(cc2);//System.out.println(cc.toString());

    }

}

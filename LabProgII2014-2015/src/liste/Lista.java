package liste;

public class Lista {
	
	private Node first;
	
	public class Node{
		private int dato;
		private Node next;
		
		public Node(){
			set(0, null);
		}
		public Node(int dato){
			set(dato, null);
		}
		public Node(int dato, Node next){
			set(dato,next);
		}
		private void set(int dato, Node next){
			this.dato = dato;
			this.next = next;
		}
		public int getDato(){
			return dato;
		}
		public Node getNext(){
			return next;
		}
		public void setDato(int dato){
			set(dato, this.next);
		}
		public void setNext(Node next){
			set(this.dato, next);
		}
		public void push_Back(Node tail){
			if(next == null)
				setNext(tail);
			else
				next.push_Back(tail);
		}
		public String toString(){
			if(next == null) 
				return  dato +" )";
			else
				return dato +", " + next.toString();
		}
		public String toStringInverse(){
			if(next == null)
				return "( " + dato;
			else
				return next.toStringInverse() + ", " + dato;
		}
		public boolean cerca(int i){
			if(next == null)
				return (dato == i);
			else
				return ((dato == i)?true : next.cerca(i));
		}
		public boolean erasePosition(int position, int i){
			if(i == position-1){
				this.setNext(getNext().getNext());
				return true;
			}else{ 
				return next.erasePosition(position, i+1);	
			}
		}
		public boolean eraseInteger(int i){
			if(next == null){
				return false;
			}else{
				if(next.dato == i){
					this.setNext(next.next);
					return true;
				}else{
					return next.eraseInteger(i);
				}
			}
		}
		public Node clona(){
			Node clone = new Node(this.dato);
			return clone;
		}
		public boolean equals(Node nodo){
			if(nodo.next == null){
				return (this.dato == nodo.dato);
			}else{
				return next.equals(nodo.next);
			}
		}
		public void invertRic(Node nodo){
			if(this.next != null){
				Node aux = new Node(this.next.dato,nodo);
				nodo = aux;
				next.invertRic(nodo);
			}
		}
		
	}
	public Lista(){
		first = null;
	}
	public Lista(Node node){
		this.first = node;
	}
	/**Inserisce un elemento in testa alla lista*/
	public void push_front(Node head){
		if(isEmpty())
			first = head;
		else
			head.setNext(first); first = head;
	}
	/**Inserisce un elemento in coda alla lista*/
	public void push_back(Node tail){
		if(isEmpty())
			first = tail;
		else
			first.push_Back(tail);
	}
	/**Restituisce una stringa che rappresenta la LIsta chiamante*/
	public String toString(){
		if(isEmpty())
			return "";
		else
			return "( " +first.toString();
	}
	/**Restituisce una stringa che rappresenta la lista chiamante invertiva*/
	public String toStringInverse(){
		if(isEmpty())
			return "";
		else
			return first.toStringInverse() + " )";
	}
	/**Restitusce true se la Lista � vuota, altrimenti false*/
	public boolean isEmpty(){
		return (first == null);
	}
	/**Metodo booleano che dato un intero restituisce true se esiste un nodo con
	*tale valore all'interno della lista altrimenti false;*/
	public boolean cerca(int i){
		if(isEmpty())
			return false;
		else
			return first.cerca(i);
	}
	/**Metodo booleano, che esegue la rimozione di dell'n-esimo nodo
	 * restitusce true se l'operazione � riuscita, altrimenti false*/
	public boolean erasePosition(int position){
		if(( isEmpty() ) || (position > size()) || (position <= 0))
			return false;
		else
			if(position == 1){
				first = first.getNext();
				return true;
			}else{
				return first.erasePosition(position, 1);
			}
	}
	/**eraseInteger(int i) rimozione del nodo contenente un certo intero;*/
	public boolean eraseInteger(int i){
		if(isEmpty() || !cerca(i)){
			return false;
		}else{
			if(first.getDato() == i){
				first = first.getNext();
				return true;
			}else{
				return first.eraseInteger(i);
			}
		}
	}
	/**Metodo che effettua la rimozione di tutti i nodi contenenti un certo intero;*/
	public boolean eraseAllInterger(int i){
		boolean canc = false;
		if(isEmpty() || !cerca(i)){
			return canc;
		}else{
			if(first.getDato() == i){
				first = first.getNext();
				canc = true;
			}
			while(cerca(i)){
				canc = first.eraseInteger(i);
			}
		}return canc;
	}
	public boolean equals(Lista l2){
		if((first == null) && (l2.first == null)){
			return true;
		}
		if(this.size() != l2.size() ){
			return false;
		}else{
			return first.equals(l2.first);
		}
	}
	/**invert() inversione di una lista creando una nuova lista.*/
	public Lista invert(){
		Lista l = new Lista();
		Node iterator = first;
		if(isEmpty()){
			return null;
		}
		while(iterator != null){
			l.push_front(iterator.clona());
			iterator = iterator.getNext();
		}
		return l;
	}
	public Lista invertRic(){
		Lista l = new Lista(first.clona());
		first.invertRic(l.first);
		return l;
	}
	
	public int size(){
		Node iterator = first; int cont = 0;
		if(isEmpty())
			return cont;
		else{
			while(iterator != null){
				cont++;
				iterator = iterator.getNext();
			}
			return cont;
		}
	}
	
	
	
}

package liste;

import liste.Lista.Node;

public class TestLista {
	public static void main(String[] args){
		Lista l = new Lista(new Lista().new Node(1,new Lista().new Node(2, new Lista().new Node(3))));
		
		
		Node uno = new Lista().new Node(1);
		Node due = new Lista().new Node(2);
		Node tre = new Lista().new Node(3);
		Node quattro = new Lista().new Node(4);
		Node cinque = new Lista().new Node(5);
		Node sei = new Lista().new Node(6);
		l.push_back(quattro);
		l.push_back(cinque);
		l.push_back(sei);
	
		
		
		System.out.println(l);
		System.out.println(l.toStringInverse());
		
		int val = 6;
		System.out.println("Prova del metodo cerca con il valore "+ val
				+ "\n****************************");
		System.out.println(((l.cerca(val))?"Valore trovato": "Non trovato"));
		
		int pos = 5;
		System.out.println("Prova cancella nodo in posizione "+ pos
				+ "\n****************************");
		//System.out.println(((l.erasePosition(pos))?"Valore cancellato": "Non trovato"));
	
		System.out.println(l);
		int intero = 4;
		System.out.println("Prova cancella nodo con il dato "+ intero
				+ "\n****************************");
		//System.out.println(((l.eraseInteger(intero))?"Valore cancellato": "Non trovato"));
		System.out.println(l);
		
		l.push_back(uno);
		l.push_back(due);
		l.push_back(tre);
		
		System.out.println(l);
		System.out.println("Prova cancella i nodi con il dato "+ intero
				+ "\n****************************");
		System.out.println(((l.eraseAllInterger(intero))?"Valori cancellati": "Non trovato"));
		
		System.out.println(l);
		
		Lista l2 = new Lista(new Lista().new Node(2));
		Lista l3 = new Lista(new Lista().new Node(2));
		
		System.out.println(l.invertRic());
		
		System.out.println(l2.equals(l3));
		
	}
}

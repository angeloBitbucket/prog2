package ccBanca;

public class ContoCorrente {
	private double saldo;
	private int idNumber;
	private String idName;
	private static int idNumberStatic = -1;
	
	public ContoCorrente(){
		set(0,"Nessun Nome");
	}
	public ContoCorrente(String idName){
		set(0,idName);
	}
	public ContoCorrente(ContoCorrente cc){
		set(cc.saldo, cc.idName);
	}
	public ContoCorrente(double saldo){
		set(saldo, "Nessun Nome");
	}
	public ContoCorrente(double saldo, String idName){
		set(saldo, idName);
	}
	//metodi setter
	public void setSaldo(double saldo){
		this.saldo = saldo;
	}
	public void setIdName(String idName){
		this.idName = idName;
	}
	
	public void setConto(double saldo, String idName){
		this.saldo = saldo;
		this.idName = idName;
	}
	private void set(double saldo, String idName){
		this.saldo=saldo;
		this.idName = idName;
		
		// Il ID Number deve essere possitivo e essere di 4 cifre
		idNumberStatic += 1;
		this.idNumber = idNumberStatic;
	}
	// metodi getter
	public double getSaldo(){
		return saldo;
	}
	public int getIdNumber(){
		return idNumber;
	}
	public String getIdName(){
		return idName;
	}
	
	//metodo per stampare un oggetto ContoCorrente
	public String toString(){
		return "\nNumero di Conto: " + idNumber + "\nNome Intestatario: " + idName + 
				"\nSaldo del conto: � " + saldo;
	}
	//metodo per depositare
	
	public void deposito(double depo){
		setSaldo(saldo+depo);
	}
	
	//metodo per prelevare, in questo metodo deve anche essere gestito il caso in cui non si possa effettuare il prelievo
	//perche' non ci sono abbastanza soldi sul conto (restituendo un booleano);
	public boolean prelevare(double prele){
		if(prele > saldo){
			System.out.println("Saldo insufficiente");
			return false;
		}else{
			setSaldo(saldo-prele);
			return true;
		}
	}
	public static void setContatore(int num){
		idNumberStatic = num;
	}
	public int compareTo(Object o){
		if((o != null) && (o instanceof ContoCorrente)){
			ContoCorrente altroConto = (ContoCorrente)o;
			return (idName.compareTo(altroConto.idName));
		}
		return -1;
	}
	public boolean equals(Object o){
		if((o != null) && (o instanceof ContoCorrente)){
			ContoCorrente altroConto = (ContoCorrente)o;
			return (this.idName.equals(altroConto.idName));
		}
		return false;
	}
}

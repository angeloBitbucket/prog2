package ccBanca;

public class TestBanca {

	public static void main(String[] args) {
		Banca miaBanca = new Banca("Angelo", 10);
		
		System.out.println(miaBanca);
		
		System.out.println("\n Prova dei metodi apriConto"
				+ "\n*******************************\n");
		miaBanca.apriContoCorrente("Angelo");
		
		miaBanca.apriContoCorrente("Pipo", 5000.99);
		
		ContoCorrente mioConto = new ContoCorrente(19239.99,"Patric");
		miaBanca.apriContoCorrente(mioConto);
		
		System.out.println(miaBanca);
		
		System.out.println("\n Prova del metodo getConto"
				+ "\n*******************************\n");
		int idNumber = 2; //prova con idNumber da 0 a 4
		System.out.println((miaBanca.getConto(idNumber) == null)?"Non esiste questo Conto":miaBanca.getConto(idNumber));
		
		System.out.println("\n Prova del metodo setSaldo"
				+ "\n*******************************\n");
		
		System.out.println((miaBanca.setSaldo(1000, idNumber))?"operazione riuscita":"conto non esistente");
		System.out.println(miaBanca.getConto(idNumber));
		
		//System.out.println(miaBanca);
		System.out.println("\n Prova del metodo getSaldoTotale"
				+ "\n*******************************\n");
		System.out.println("Il saldo totale della banca �: "+miaBanca.getSaldoTotale());
		
		System.out.println("\n Prova del metodo trasferire"
				+ "\n*******************************\n");
		miaBanca.trasferire(2, 4, 1000);
		
		Banca tuaBanca = new Banca("Fideuran", 10);
		
		tuaBanca.apriContoCorrente("Lidia", 10254);
		
		tuaBanca.apriContoCorrente("Elena", 8254);
		
		tuaBanca.apriContoCorrente("Ivana", 10254);

		tuaBanca.apriContoCorrente("Lady", 1254);
		
		
		System.out.println(tuaBanca);
	
		miaBanca.apriContoCorrente("Io", 100);
		
		miaBanca.apriContoCorrente("Tu", 100);
		
		miaBanca.apriContoCorrente("Jula", 100);
		
		miaBanca.apriContoCorrente("Betto", 100);
		
		miaBanca.apriContoCorrente("angelo", 2345);
		
		miaBanca.apriContoCorrente("Angelo", 10000);

		System.out.println(miaBanca);
		
		tuaBanca.selectionSort();
		miaBanca.selectionSort();
		
		System.out.println("\n Prova del metodo merge"
				+ "\n*******************************\n");
		System.out.println(miaBanca.merge(tuaBanca));
		
		
		//System.out.println(miaBanca);
		
		System.out.println("\n Prova del metodo sommaSaldo"
				+ "\n*******************************\n");
		String s = "Angelo";
		System.out.println("\nIl Saldo totale del signor " + s +" �: \n");
		System.out.println(miaBanca.sommaSaldi(s));
	}
}

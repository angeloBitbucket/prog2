package ccBanca;

public class BancaOrdinata extends Banca {
	// merge sort
	
	public void selectionSort(){
		System.out.println("Ordina per saldo?  true o false");
		boolean ordinaPerSaldo = tastiera.nextBoolean();
		for(int indice = 0; indice < this.getLast();indice++){
			int indiceDelSuccPiuPiccolo = getIndiceDelPiuPiccolo(indice, conti(), ordinaPerSaldo);
			scambio(indice, indiceDelSuccPiuPiccolo, conti());
		}
	}
	public int getIndiceDelPiuPiccolo(int indiceInizio, ContoCorrente[] cc, boolean ordinaPerSaldo){
		ContoCorrente minimo = cc[indiceInizio];
		int indiceDelMinimo = indiceInizio;
		if(ordinaPerSaldo){
			for(int indice = indiceInizio + 1; indice < this.getLast(); indice++){
				if(cc[indice].getSaldo() < minimo.getSaldo()){
					minimo = cc[indice];
					indiceDelMinimo = indice;
				}
			}
		}else{
			for(int indice = indiceInizio + 1; indice < this.getLast(); indice++){
				if(cc[indice].compareTo(minimo) < 0){
					minimo = cc[indice];
					indiceDelMinimo = indice;
				}
			}
		}
		
		return indiceDelMinimo;
	}
	public void scambio(int i, int j, ContoCorrente[] cc){
		ContoCorrente temp = cc[i];
		cc[i] = cc[j];
		cc[j] = temp;
	}
	
	//merge
	public Banca merge(Banca bb){
		int m = this.getLast(); int n = bb.getLast();
		Banca nuovaBanca = new Banca("Banca fusa", m+n);
		int i = 0, j = 0;
		while(i < m && j < n){
			if(this.conti()[i].getSaldo() <= bb.conti()[j].getSaldo()){
				nuovaBanca.conti()[i + j] = this.conti()[i];
				i++;
			}else{
				nuovaBanca.conti()[i + j] = bb.conti()[j];
				j++;
			}
		}
		while(i < m){ nuovaBanca.conti()[i + j] = this.conti()[i]; i++;}
		while(j < n){ nuovaBanca.conti()[i + j] = bb.conti()[j]; j++;}
		return nuovaBanca;
	}
	
}

package ccBanca;
import java.util.*;

public class Banca implements BancaInterface{
	/*1. nome della banca
2. totale saldo della banca
3. un array ContoCorrente[] che contiene tutti i conti correnti creati dalla banca (si utilizzi
come dimensione dell'array un numero abbastanza grande da contenere diversi conti
correnti per eseguire delle prove)*/
	
	private String name;
	private double saldoTotale;
	private ContoCorrente[] conti;
	private int numConti;
	
	Scanner tastiera = new Scanner(System.in);
	
	//Costruttori
	public Banca(){
		name = "Nessun Nome";
		conti = new ContoCorrente[0];
		numConti = 0;
	}
	public Banca(String name, int length){
		this.name = name;
		this.conti = new ContoCorrente[length];
		this.numConti = 0;
		ContoCorrente.setContatore(-1);
	}
	
	/*� per inserire un nuovo conto corrente (tre metodi possibili):
	1. apriContoCorrente(String nomeCorrentista, double versamento)
	2. apriContoCorrente(String nomeCorrentista)
	3. apriContoCorrente(ContoCorrente cc)*/
	
	public void apriContoCorrente(String nomeCorrentista, double versamento){
		set(nomeCorrentista, versamento);
	}
	public void apriContoCorrente(String nomeCorrentista){
		set(nomeCorrentista, 0);
	}
	public void apriContoCorrente(ContoCorrente cc){
		conti[getLast()] = cc;
		saldoTotale += cc.getSaldo();
		numConti += 1;
	}
	private void set(String nome, double versamento){
		
		conti[this.getLast()] = new ContoCorrente(versamento, nome);
		saldoTotale += versamento;
		numConti += 1;
	}
	public int getLast(){
		int i = 0;
		while(conti[i] != null){
			i++;
		}
		return i;
	}
	public ContoCorrente[] conti(){
		return conti;
	}
	
	//metodo per accedere ad un conto dato il numero del conto,
	//restituisce null se il conto cercato non esiste, altrimenti restituisce il riferimento al conto.
	public ContoCorrente getConto(int number){
		return ((conti[number]==null)? null: conti[number]);
	}
	//metodo per modificare direttamente il saldo di un conto dato il suo numero identificativo;
	//restitusce false se il conto cercato no esiste, altrimenti restitusce true.
	public boolean setSaldo(double saldo, int idNumber){
		if(conti[idNumber]== null){
			return false;
		}
		saldoTotale -= conti[idNumber].getSaldo();
		conti[idNumber].setSaldo(saldo);
		saldoTotale += saldo;
		return true;
	}
	//per ottenere il totale dei soldi della banca (somma dei saldi di tutti i conti);
	public double getSaldoTotale(){
		return saldoTotale;
	}
	//per trasferire da un conto ad un altro una certa somma di denaro;
	//restitusce false se almeno uno dei conti non esiste o se il trasferimento falisce per mancanza di fondi,
	//altrimenti restituisce true;
	public boolean trasferire(int idUno, int idDue, double somma){
		if((conti[idUno] != null) && (conti[idDue] != null)){	
			if(!conti[idUno].prelevare(somma)){
				return false;
			}else{
				conti[idDue].deposito(somma);
				return true;
			}
		}return false;
	}
	public String toString(){
		int i = 0; String s = "Banca " + name +"\n";
		while(i < getLast()){
			s = s + conti[i].toString()+"\n";
			i++;
		}
		return s;
	}
	
	public void selectionSort(){
		System.out.println("Ordina per saldo?  true o false");
		boolean ordinaPerSaldo = tastiera.nextBoolean();
		for(int indice = 0; indice < this.getLast();indice++){
			int indiceDelSuccPiuPiccolo = getIndiceDelPiuPiccolo(indice, conti, ordinaPerSaldo);
			scambio(indice, indiceDelSuccPiuPiccolo, conti);
		}
	}
	public int getIndiceDelPiuPiccolo(int indiceInizio, ContoCorrente[] cc, boolean ordinaPerSaldo){
		ContoCorrente minimo = cc[indiceInizio];
		int indiceDelMinimo = indiceInizio;
		if(ordinaPerSaldo){
			for(int indice = indiceInizio + 1; indice < this.getLast(); indice++){
				if(cc[indice].getSaldo() < minimo.getSaldo()){
					minimo = cc[indice];
					indiceDelMinimo = indice;
				}
			}
		}else{
			for(int indice = indiceInizio + 1; indice < this.getLast(); indice++){
				if(cc[indice].compareTo(minimo) < 0){
					minimo = cc[indice];
					indiceDelMinimo = indice;
				}
			}
		}
		
		return indiceDelMinimo;
	}
	public void scambio(int i, int j, ContoCorrente[] cc){
		ContoCorrente temp = cc[i];
		cc[i] = cc[j];
		cc[j] = temp;
	}
	
	//merge
	public Banca merge(Banca bb){
		int m = this.getLast(); int n = bb.getLast();
		Banca nuovaBanca = new Banca("Banca fusa", m+n);
		int i = 0, j = 0;
		while(i < m && j < n){
			if(this.conti[i].getSaldo() <= bb.conti[j].getSaldo()){
				nuovaBanca.conti[i + j] = this.conti[i];
				i++;
			}else{
				nuovaBanca.conti[i + j] = bb.conti[j];
				j++;
			}
		}
		while(i < m){ nuovaBanca.conti[i + j] = this.conti[i]; i++;}
		while(j < n){ nuovaBanca.conti[i + j] = bb.conti[j]; j++;}
		return nuovaBanca;
	}
	
	/*dato il nome di un cliente della banca, identificare tutti i conti correnti di sua propriet� e 
	 * restituire la sua disponibilit� complessiva (somma dei saldi dei suoi conti corrente)
	Il metodo deve funzionare anche quando al nome in questione non corrisponde alcun conto corrente
	 e quando al nome in questione corrisponde un solo conto corrente (anzich� molti)*/
	public double sommaSaldi(String idName){
		int sommaSaldi = 0;
		for(int i = 0; i < getLast(); i++){
			if(conti[i].getIdName().equalsIgnoreCase(idName)){
				sommaSaldi += conti[i].getSaldo();
			}
		}return sommaSaldi;
	}
	
	
}

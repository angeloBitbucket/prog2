package ccBanca;

public interface BancaInterface {
	
	public void apriContoCorrente(String nomeCorrentista, double versamento);
	public void apriContoCorrente(String nomeCorrentista);
	public void apriContoCorrente(ContoCorrente cc);
	public ContoCorrente getConto(int number);
	public boolean setSaldo(double saldo, int idNumber);
	public double getSaldoTotale();
	public boolean trasferire(int idUno, int idDue, double somma);
	public String toString();
	public void selectionSort();
	public int getIndiceDelPiuPiccolo(int indiceInizio, ContoCorrente[] cc, boolean ordinePerSaldo);
	public void scambio(int i, int j, ContoCorrente[] cc);
	
}



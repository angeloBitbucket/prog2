package ccBanca;

public class TestContoCorrente {
	
	public static void main(String[] args){
		
	
		ContoCorrente tuoConto = new ContoCorrente(450.32, "Angelo Rodry");
		System.out.println(tuoConto);
		
		System.out.println("\nProva metodi setter\n"
				+ "*********************************");
		tuoConto.setConto(0, "Nessun Nome");
		System.out.println(tuoConto);
		
		tuoConto.setSaldo(986.25);
		System.out.println(tuoConto);
		
			
		tuoConto.setIdName("Angelo");
		System.out.println(tuoConto);
		
		System.out.println("\nProva metodi getter\n"
				+ "*********************************");
		System.out.println("Il nome del intestatario �: ");
		System.out.println(tuoConto.getIdName());
		System.out.println("Il numero identificativo e:");
		System.out.println(tuoConto.getIdNumber());
		System.out.println("Il saldo �: ");
		System.out.println(tuoConto.getSaldo());
		
		System.out.println("\nProva del idNumber\n"
				+ "*********************************");
		
		ContoCorrente mioConto = new ContoCorrente(490.32, "mio COnto");
		System.out.println(mioConto);
		
		ContoCorrente nostroConto = new ContoCorrente(7838.32, "Nostro conto");
		System.out.println(nostroConto);
		
		ContoCorrente ilConto = new ContoCorrente(9800.45, "il conto");
		System.out.println(ilConto);
		
		System.out.println("\nProva metodo deposito\n"
				+ "*********************************");
		mioConto.deposito(1000);
		nostroConto.deposito(1000);
		ilConto.deposito(1000);
		
		System.out.println("Deposito di 1000 � per i conti 9002, 9003 e 9004\n");
		System.out.println(mioConto);
		System.out.println(nostroConto);
		System.out.println(ilConto);
		
		System.out.println("\nProva metodo deposito\n"
				+ "*********************************");
		
		System.out.println("Tentativo di Prelievo di 2000 � per i conti 9002, 9003 e 9004\n");
		System.out.println((mioConto.prelevare(2000))?"\nTentativo riuscito"+mioConto:"Tentativo non riuscito");
		System.out.println((nostroConto.prelevare(2000))?"\nTentativo riuscito"+nostroConto:"Tentativo non riuscito");
		System.out.println((ilConto.prelevare(2000))?"\nTentativo riuscito"+ilConto:"Tentativo non riuscito");
		
		
		
	}
}
